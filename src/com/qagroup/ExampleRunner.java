package com.qagroup;

import java.util.ArrayList;
import java.util.List;

public class ExampleRunner {
	
	static int[] intArray = new int[] {4,6,5,7,8,11};
	
	public static void main(String[] args) {
		System.out.println("Sum: " + calcSum());
		System.out.println("Average: " + calcAverage());
	
//		Car car = new Car();
//		List link = car.drive(); 
//		System.out.println(car.equals(new Car()));
	}

	public static double calcAverage() {
		//int[] intArray = new int[] {4,6,5,7,8,11};
//		int sum = 0; 
//		for(int i = 0; i < 6; i++) {
//			sum = sum + intArray[i];
//			
//			//System.out.println(intArray[i]);
//		}
		return calcSum() / intArray.length;
	}
	
	public static double calcSum() {
		int sum = 0; 
		for(int i = 0; i < intArray.length; i++) {
			sum = sum + intArray[i];
		}
		return sum ;
	}
		
}
