package com.qagroup;

public class FirstClass {
	
	public static void main(String[] args) {
		String[] stringArr = getStringArr();
		
		System.out.println(stringArr.length);
		
		for(String str : stringArr) {
			System.out.println(str.toLowerCase());	
		}
		System.out.println("\n=======\n");
		
		for(int i = 0; i < stringArr.length; i++) {
			System.out.println(stringArr[i].toUpperCase());
		}
		
		printElement(stringArr, -1);
		printElement(stringArr, 2);
		printElement(stringArr, -4);
		
	}
	
	public static void printElement(String[] arr, int index) {
		if (index >= 0 && index < arr.length) {
			System.out.println(arr[index]);
			return;
		}	
		if (index < 0 && index >= -arr.length) { 
			System.out.println(arr[arr.length + index]);
		}
		
		throw new ArrayIndexOutOfBoundsException("Index should be in the range between [-" + arr.length + " : " + arr.length + ")");
		
	}
	
	public static String[] getStringArr() {
		String[] stringArr = new String[4];
		stringArr[0] = "Hello";
		stringArr[1] = "Hi";
		stringArr[2] = "Hola";
		stringArr[3] = "Greetings";
		
		return stringArr;
	}
	
	public static void main3(String[] args) {
		String s = "Hello World!";
		System.out.println(s.toUpperCase());
		// main2(args);
		// main2(null);
		int a1 = 2;
		int sqA = square(a1);
		System.out.println(sqA);
		
		Integer i1 = new Integer(10);
		Double d2 = new Double(2.5);
		
		i1 = a1;
		a1 = i1;
	}
	
	public static int square(int a) {
		return a * a;
		
	}
	
	public static void main2(String[] args) {
		int a1 = 234;
		
		System.out.println(a1);
		
		a1 = -100;
		System.out.println(a1);
		
		double d1 = 2.5;
		System.out.println(d1);
		
		boolean b1 = true;
		System.out.println(b1);
		
		int a2 = a1;
		System.out.println("a2: " + a2);
		
		String s = "text";
		System.out.println("s: " + s);
		
		s = "2";
		a1 = 2;
		System.out.println("s + a1:");
		System.out.println(s + a1);
		System.out.println("a1 + s:");
		System.out.println(a1 + s);
		
		a2 = 5;
		System.out.println(a1 + a2);
		
		s = "Hello World!";
		System.out.println(s.length());
		
		
	}

}
