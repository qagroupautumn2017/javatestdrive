package com.qagroup;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kondratievy
 *
 */
public class Car {
	
	private String brand;
	
	private String color;
	
	private int currentSpeed;
	
	private static int[] intArray = new int[] {4,6,5,7,8,11};
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brand == null) ? 0 : brand.hashCode());
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + currentSpeed;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (brand == null) {
			if (other.brand != null)
				return false;
		} else if (!brand.equals(other.brand))
			return false;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (currentSpeed != other.currentSpeed)
			return false;
		return true;
	}



	public List drive() {
		for (int i = 6; i < intArray.length; i++) {
			System.out.println(intArray[i]);
			
		}
		List<String> list = new ArrayList<String>();
		list.add("1");
		list.add("2");
		list.add("3");
		for (String s: list) {
			System.out.println(s.equals(list));
			System.out.println("S");
		}
		return list;
	}
	
}

